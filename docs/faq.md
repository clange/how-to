Frequently Asked Questions
==========================

### Q1: Why do I get a _403_ error when I'm connecting to https://myservice.docs.cern.ch which I have just deployed?

A1: You must have an _index.md_ file in your _docs_ directory for your documentation to be displayed. More info [here](https://www.mkdocs.org/user-guide/writing-your-docs/#index-pages).

### Q2: Can I import existing Markdown documentation from GitHub and repositories?

A2: Yes, GitLab allows to sync GitHub repos. You could make use of git _submodules_ to achieve import.

### Q3: How to make LaTeX work in a MkDocs documentation?

A3: See [this dedicated page](advanced/latex.md) explaining what to add.

### Q4: I need some help with Markdown syntax, especially _nested lists_.

A4: Have a look at [https://wordpress.com/support/markdown-quick-reference/](https://wordpress.com/support/markdown-quick-reference/) and  [https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#lists](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#lists) and [https://daringfireball.net/projects/markdown/syntax](https://daringfireball.net/projects/markdown/syntax)

### Q5: Why sometimes changes in .md files are not visible?

A5: Your _mkdocs.yml_ file might be incorrect. Indentation in YAML is very important. You can use an online tool like [https://codebeautify.org/yaml-validator](https://codebeautify.org/yaml-validator) to validate the mkdocs.yml contents.

### Q6: I am trying to clone my GitLab repository from which my .docs.cern.ch documentation is generated but I get an error.

A6: This is probably because you are using `https` as the clone method and the project is not public. The project can be only cloned by authenticated users. You can either provide the authentication credentials or just make the project public so that it can be cloned.

### Q7: May I give different access rights for different files or subdirectories of my .docs.cern.ch site, if possible based on e-groups?

A7: Unfortunately, this is not possible right now. The only way to do it would be to have a single repository for all the docs along with gitlab CI that would build different folders for different sites and redeploy the OpenShift applications. You will have separate site.docs.cern.ch , site-admin.docs.cern.ch, site-ops.docs.cern.ch etc. You can set access permissions according to the authorised readers.

### Q8: I added _SSO proxy_ allowing only an e-group, which contains another e-group (I'm a member, of course). When I try to reach my .docs.cern.ch site, I get error _401 Unauthorized_. Why?

A8: The problem is on the e-groups side. Nested e-groups should indeed not pose a problem. What might be the issue is a delay in synchronising the e-group members (for newly created e-groups). Logging in via a private browser window might also provide an indication as to where the problem might be. If the problem is not solved overnight, please get in touch with ServiceNow and hint that the issue might be with e-groups / SSO. Please note that this process will change in the new .docs.cern.ch set-up.

### Q9: Can I use `navigation.instant`?

A9: If your documentation site is behind CERN SSO (`anonymous` checkbox is unchecked) and it is using `material` theme, **you can not use [`navigation.instant`](https://squidfunk.github.io/mkdocs-material/setup/setting-up-navigation/#instant-loading){target=_blank} as a feature** of the `theme` configuration on your `mkdocs.yml` configuration file.
With `navigation.instant` feature enabled, all links are intercepted and dispatched via XHR which prevents a full page reload. This, unfortunately doesn't work well when a given request returns a redirect because the plugin responsible for that feature, apparently doesn't follow them causing the site to be partially broken.

### Q10: How can I use a reserved hostname on my site?

!!! info

    `Reserved` in this context means that a given name can't be used without a prior approval from the cluster admins.

A10: Web services portal by default validates the name of the site (the one you provide in the [GitLab Pages site creation form](https://webservices-portal.web.cern.ch/gps){target=_blank}) against a predefined list of `reserved` names. In order to avoid this limitation, create the GitLab Pages site via its [creation form](https://webservices-portal.web.cern.ch/gps){target=_blank} using another name (for example `my-project-docs` as a site name) and create a [SNOW Request](https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&se=gitlab-pages){target=_blank} asking for this limitation to be lifted off on your project.

!!! note
    The value for the site name is also used for the name of the OKD project available on webeos.cern.ch (as the infrastructure available on [webeos.cern.ch](https://webeos.cern.ch/){target=_blank} is used to host all the Gitlab Pages sites). The same check is also performed against the project name. In order to lift off this limitation, please, open a [SNOW Request](https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&se=gitlab-pages){target=_blank}.

In case your project is already created and you want to start using a reserved hostname for it, please create a [SNOW Request](https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&se=gitlab-pages){target=_blank} specifying the project name and the desired site name.
