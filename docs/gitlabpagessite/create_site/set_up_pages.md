# Set up GitLab Pages

Great! You are almost done. Now that you have also created your [Web Services site](https://webservices-portal.web.cern.ch/my-sites){target=_blank} you can configure the GitLab Pages of your documentation repository.

!!!warning
    At this point, you should already [have built the artifacts](/gitlabpagessite/create_site/creategitlabrepo/create_with_mkdocs/#build-artifacts-for-an-imported-project){target=_blank} for your site, meaning that a Pipeline was already ran previously. Make sure that this is the case and that you can see, at least, one successful Pipeline (with `pages` and `pages:deploy` jobs) similar to the one in the image below:

    ![pipelines](/images/gitlab-build-pipelines.png)

    with the following 2 jobs:

    ![jobs](/images/gitlab-build-jobs.png)

    Otherwise, following the next steps will lead to differences between what you see and this documentation. If you see a page similar to the one shown on the image below, it means that the artifacts for your site haven't been built properly.

    ![missing-pipelines](/images/gitlab-missing-run-pipeline.png)


## Enable Pages in a repository

Now, it is time to go back to the [GitLab repository](https://gitlab.cern.ch/) that contains your documentation files. You will now see a new option in the `Deploy` section of your project called `Pages`.

![deploy](/images/gitlab-deploy-pages.png)

!!! info
    To be able to see `Pages` you need to have, at least, a Maintainer role.


## Add a custom Pages domain

First of all, make sure that `Force HTTPS (requires valid certificates)` is ticked off, and then click on the `Save changes` blue button. Your site will be accessible via HTTPS in any case thanks to the new infrastructure.

![certificates](/images/gitlab-https-pages.png)

In order to create a custom Pages domain, you have to click on the upper right button **New Domain**.
Now, to create your domain please, use the same URL that you used before for the **Host** when you [created your site](/gitlabpagessite/create_site/create_webeos_project/#create-a-site).

![domain](/images/gitlab-domain-new-site.png)

!!! warning
    There are some restrictions in order to create your domain:

    * Domain (`my-service-name-docs` in the previous image) only allows letters, numbers, and hyphen symbol ( - ).
    * No hostname can live under a domain that is not supported (`docs.cern.ch` and `web.cern.ch` are allowed; we strongly suggest to use `docs.cern.ch` for new sites and `web.cern.ch` for sites that used the domain in the old infra).
    * No hostname can have its first segment being shorter than 4 characters.


You can leave the **Certificate (PEM)** and **Key (PEM)** fields empty if you have previously disabled the button `Force HTTPS (requires valid certificates)`.

Once you are happy, click on **Create New Domain** button.


![confirmation](/images/gitlab-domain-new-site-confirmation.png)

A confirmation page will appear. Click on the button **Save Changes**. If everything goes well, you will see the new domain in the `Domains` list. Your new site should be available in a few minutes.


## Access restriction

If your site is protected with CERN SSO and you want to restrict the access to your site only to members of specific groups, then you can follow the instructions described [here](../../advanced/restricted_egroups.md).


## Done!

Congratulations, your site is ready! You can access it through the URL that you set as a domain. If you want some tips about how to create files in GitLab you can [check this section](/gitlabpagessite/create_site/create_new_file/).
