# Create a new project on [Web Services](https://webservices-portal.web.cern.ch){target=_blank}

Once your GitLab repository is fully configured, go to the [GitLab Page site creation form](https://webservices-portal.web.cern.ch/gps){target=_blank} to create a new site.

This will show a form with the following mandatory fields:

* **Category:** There are three categories available for the site. By default, `Official` category is selected in the dropdown menu, which is the category used in the following example.

* **Site name:** Enter the name of the site (i.e. `my-service-name-docs`).

    !!! warning
        Notice that you do not have to put the `http/s` protocol at the beginning.

    The **Subdomain** of the site selected by default in the dropdown menu is `.docs.cern.ch`, which is the subdomain used in the following example and the recommended one. Along with the **Site name**, it will make up the `host` (URL) of the site (i.e. `my-service-name-docs.docs.cern.ch`).

    !!! warning
        * Domain (`my-service-name-docs` in the example) only allows letters, numbers, and hyphen symbol ( - ).
        * No hostname can live under a domain that is not supported (`docs.cern.ch` and `web.cern.ch` are allowed; we strongly suggest to use `docs.cern.ch` for new sites and `web.cern.ch` for sites that used the domain in the old infra). In the rare case that you need a custom domain please, open a [SNOW Request](https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&se=gitlab-pages){target=_blank}.
        * No hostname can have its first segment being shorter than 4 characters.

    !!! note
        An error may occur when a reserved name is used for the site name. The procedure to follow in this case is described in the [FAQ section](/faq/#q10-how-can-i-use-a-reserved-hostname-on-my-site).

* **Description**: Enter a description of the site. You can change it later on.

You also need to accept the CERN Computing Rules by clicking on the checkbox.

![creation form](/images/webservices-portal-create-gps-form.png)

Once you have filled in the form, click on the **Create** button. If everything goes as expected you will see a page similar to this:

![success form](/images/webservices-portal-success-gps.png)


## Next step 

Perfect! Your site in Web Services is already configured and there is already a registered URL (the **host**, i.e. `my-service-name-docs.docs.cern.ch`) from which people will be able to access your site. Remember this URL (host), because it is important for [the next step while setting up GitLab Pages](/gitlabpagessite/create_site/set_up_pages/).
