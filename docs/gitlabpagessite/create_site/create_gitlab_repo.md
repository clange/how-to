# Create a GitLab repository

First of all, you need to create a new repository on [GitLab](https://gitlab.cern.ch/){target=_blank} to host your documentation files.

You can use **one** of these options:

**A.** [Markdown along with MkDocs](/gitlabpagessite/create_site/creategitlabrepo/create_with_mkdocs/): Recommended solution for static sites.

**B.** [Static Site Generators (SSGs) that GitLab offers as templates](/gitlabpagessite/create_site/creategitlabrepo/create_with_template/): The new infrastructure comes with this new nice feature, which allows you to select between several ways of writing your documentation.
