# Modify your site's settings

To manage your site you can use the [Web Services Portal](https://webservices-portal.web.cern.ch){target=_blank} by clicking on the site that you want to manage. This will open a form with a general management panel and a custom `Served From` panel from which you can manage the site, as you can see in the following image:

![manage gps](/images/webservices-portal-manage-gps.png)

- **Site URL**: The URLs (hosts) of the site. If you change both the site name or the subdomain of any of the URLs you need to update the [custom Pages domain](/gitlabpagessite/create_site/set_up_pages/#add-a-custom-pages-domain){target=_blank}. You can add and/or remove URLs (hosts) in this section to make your site accessible from different URLs.

    !!! note "Custom subdomains"
        If you are using a custom subdomain, this host will appear as a disabled input. If you want to edit or remove it please, open a [SNOW Request](https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&se=gitlab-pages){target=_blank} for this purpose.

![manage gps multihost](/images/webservices-portal-manage-gps-multihost.png)


- **Guest Access**: Toggle to the right to allow anonymous access to the site or to the left to ask users to log in to the CERN SSO to be able to access the site.

    !!! info
        All URLs will have the same **Guest Access** configuration.


## Exceptional cases

If for some reason, your projects were created or have been managed via [WebEOS console](https://webeos.cern.ch/){target=_blank}, it could be the case that:

### A. Your Project doesn't contain any GitLab Pages site

A similar error to the following will be displayed on the Project's management page:

![empty project](/images/webservices-portal-manage-gps-empty-error.png)

It means that the Project exists on WebEOS, but no GitLab Pages site resource has been created yet.
In order to create it, go to the project on the [WebEOS console](https://webeos.cern.ch/){target=_blank}. Once you are in the project, on the left bar do **one** of the following (depending on if you are still in the `Administrator` view or you moved to `Developer` view):

* **A.** If you are still in the `Administrator` view, go to the **Operators** -> **Installed Operators**. This will show the list of all available operators. Make sure that you are on the correct `Project`.
![installed_operators](/images/openshift-project-installed-operators-admin.png)

* **B.** If you have changed the view before and now you are in the `Developer` view, navigate to **+Add** -> **Operator backed**. This will show the list of all available operators. Make sure that you are on the correct `Project`.
![installed_operators](/images/openshift-project-installed-operators-developer.png)

!!! warning
    Due to some changes introduced in the recent OKD version, `GitLab Pages Site Operator` may appear on the list after a couple of minutes, so please wait if it is not visible right away.

Search for `GitLab Pages Site Operator` and click on it. This will open the operator description, similar to the following image:

![operator](/images/openshift-project-create-site-operator.png)

To create your new site click on `Create instance` at the bottom of the box or go to the **Publish a static site from a GitLab repository (gitlab.cern.ch)** tab in the top bar. Click on the radio button `Current namespace only`. This will show the list of your sites created in this project (if any) and a `Create GitlabPagesSites` blue button in the top-right part. Click on this button and it will open a form.

#### Creation form

* **Name:** This is the name of your site. Since this name has to be unique we recommend you to use the same name as you have used as a project name (i.e. `my-service-name-docs`), but since this is just a name for the custom resource, this will not have further implications.
* **Labels (optional):** Optional labels to give more information about your site. You can leave it empty.
* **Hosts:** This is the list of your site's URLs that it will be accessible at. You do not need to create separate Gitlab Pages Site (GPS) for the same site, you can just use this field to declare multiple URLs for the same site. To add a new Host, click on the right arrow.

    !!! info
        Notice that you do not have to put the `http/s` protocol at the beginning.
    
    ![operator form add host](/images/openshift-project-create-site-operator-form-add-host.png)

    By clicking on `+ Add Host`, a new input box called **Value** will allow you to add another host to your site, for example, `my-redirection.docs.cern.ch`. You can repeat the same process if you want to add multiple hosts.

    ![operator form redirection](/images/openshift-project-create-site-operator-form-redirection.png)

    !!! warning
        * Domain (`my-service-name-docs` in the previous example) only allows the letters, numbers, and hyphen symbol ( - ).
        * No hostname can live under a domain that is not supported (`docs.cern.ch` and `web.cern.ch` are allowed; we strongly suggest to use `docs.cern.ch` for new sites and `web.cern.ch` for sites that used the domain in the old infra)
        * No hostname can have its first segment being shorter than 4 characters


* **Anonymous:** Tick the checkbox to make your site accessible without authentication. If the checkbox is unchecked it will redirect users to the CERN Single Sign-On in order to authenticate them before accessing your site.


Once you are done, click on **Create** and your new site will be shown in the list of your project's sites.


### B. Your Project contains several GitLab Pages site

In this case, **the first GitLab Pages site by alphabetical order will be the one displayed to manage**.

In order to remove the extra GitLab Pages sites that you are no longer interested in:

1. Select the radio button `Current namespace only`.
2. Click on the 3 dots placed on the right of each GitLab Pages site.
3. Click on `Delete GitlabPagesSite`.

![remove GPS project](/images/openshift-project-gps-remove.png)