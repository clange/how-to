# Temporarily review changes to the documentation before deploying

A limitation of the new static sites is that doing temporary reviews of your site before deploying requires a few more steps.

!!! note
    Remember that before, in order to review a MR you were supposed to include a special GitLab CI template. This template is not useful anymore thus remember to remove it from your GitLab CI configuration if it is present.


You have at your disposal several alternatives that help preview and/or check your documentation site. Depending on your needs and what you want to validate, the following alternatives are sorted by the complexity and completeness in descending order:

**A.** [Create a test/preview site](/gitlabpagessite/review/create_test_site/)

**B.** [Deploy your site locally](/gitlabpagessite/review/deploy_locally/)

**C.** [Check Markdown syntax and page appearance](/gitlabpagessite/review/syntax_and_appearance/)
