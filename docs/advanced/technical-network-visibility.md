# Technical Netwrork visibility

A GitLab Pages project may be made accessible from the Technical Network after approval from [CERN Computer Security](https://cern.ch/security).

Please open a [support ticket](https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&se=gitlab-pages)
to request it.
