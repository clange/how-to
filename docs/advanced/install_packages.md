# Install packages

It's possible to install packages needed for a MkDocs plugin by modifying the `.gitlab-ci.yml` in your repository as the example below shows:

```yaml
include:
  - project: 'authoring/documentation/mkdocs-ci'
    file: 'mkdocs-gitlab-pages.gitlab-ci.yml'

.before-script: &before-script
  # Add here the packages that you need to install
  - apt update && apt install -y libpango-1.0-0 libpangoft2-1.0-0

validation:
  before_script: *before-script

pages:
  before_script: *before-script
```
