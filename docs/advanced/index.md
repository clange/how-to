# Advanced topics

The advanced topics cover more specfic situations concerning your **Markdown-based static documentation site with [MkDocs](https://www.mkdocs.org/)**. The following topics are currently available:

* Install [custom Python packages](custom_python_packages.md) that MkDocs can use when building your documentation.
* Automatically add [the last revision date](automatic_date.md) to all your pages.
* Enable [Latex](latex.md) equations in your documentations.
* Enable [callouts](callouts.md) like notes and tips.
* Offer your documentation in [multiple languages](multi_language.md).
* Split your documentation into [multiple git repositories](multi_repositories.md).
<!--* [Test your documentation locally](local.md) before you deploy it on OpenShift.-->
<!--* [Customise your documentation with MkDocs](mkdocs.md).-->
