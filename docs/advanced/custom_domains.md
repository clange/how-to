# Custom domains

If your project is considering procuring a custom domain such as `example.org`,
it is possible to set up GitLab Pages for such a host name.

Please open a [request ticket](https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&se=gitlab-pages)
to enable support for custom domains.
