# Single language

With the introduction of the [plugin mkdocs-static-i18n](https://github.com/ultrabug/mkdocs-static-i18n) it is possible to publish your documentation in different languages. However, you may have a legacy single language site or it is enough to offer your documentation in a single language

For those with a legacy site who still want to continue with a single language, they don't have to do anything. In case for the moment you want to create a new single language site but using the i18n plugin you can follow the [multilanguage section](multi_language.md).

It is still possible to configure your site with the legacy configuration settings, for this purpose you can follow these instructions:

- You don't have to use the naming convention `<file name>.<language>.md`. Even though you are allowed to do it, ensure that you have, at least, an `index.md` file.

- [Optional] You can customize the titles of the navigation menu items via the `nav` property in the `mkdocs.yml` like in the example below (otherwise, the titles of the navigation menu items will be derived either from the document title or the file name):
```yaml
site_name: S2I MkDocs Example
site_description: S2I MkDocs Example
site_author: CERN Authoring
site_url: https://s2i-mkdocs-example.docs.cern.ch/

repo_name: GitLab
repo_url: https://gitlab.cern.ch/authoring/documentation/s2i-mkdocs-example
edit_uri: 'blob/master/docs'

theme:
    name: material

nav:
    - Introduction: index.md
    - 'Chapter 1': chapter_1.md
    - 'Chapter 2': chapter_2/index.md
    - 'MkDocs official documentation': https://www.mkdocs.org/
```